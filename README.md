# Ritardo treni

# API GraphQL
## Suggerisci stazione
```GraphQL
query{
  suggestions(searchString: "treviglio") {
    id,
    name
  }
}
```
## Trova Soluzioni
```GraphQL
query{
  trains(
    startId:"1031",
    endId:"1205",
    datetime: "2020-09-15T07:02:00"
  ) {
    durata
    vehicles {
      origine
      destinazione
      orarioPartenza
      orarioArrivo
      categoria
      categoriaDescrizione
      numeroTreno
    }
  }
}
```