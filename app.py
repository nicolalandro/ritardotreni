from flask import render_template, Flask
from flask_graphql import GraphQLView
from src.schema import schema

app = Flask(__name__, template_folder="templates", static_folder='statics')

app.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=True  # for having the GraphiQL interface
    )
)


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == "__main__":
    app.run('0.0.0.0', 5000)
