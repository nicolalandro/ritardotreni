import requests
from graphene import ObjectType, String, Schema, List, Field


class Train(ObjectType):
    name = Field(String)
    id = Field(String)


class Suggestions(ObjectType):
    suggestions = Field(List(Train), search_string=String(required=True))

    def resolve_suggestions(self, info, search_string):
        url = 'http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/autocompletaStazione/%s' % search_string
        r = requests.get(url=url)
        result = [{'name': x.split('|')[0], 'id': x.split('|')[1]} for x in r.text.strip().split('\n')]
        return result


class Vehicles(ObjectType):
    origine = Field(String)
    destinazione = Field(String)
    orarioPartenza = Field(String)
    orarioArrivo = Field(String)
    categoria = Field(String)
    categoriaDescrizione = Field(String)
    numeroTreno = Field(String)


class TrainJourney(ObjectType):
    durata = Field(String)
    vehicles = Field(List(Vehicles))


class TrainSolutions(ObjectType):
    trains = Field(List(TrainJourney), start_id=String(required=True), end_id=String(required=True),
                   datetime=String(required=True))

    def resolve_trains(self, info, start_id, end_id, datetime):
        url = 'http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/soluzioniViaggioNew/%s/%s/%s' % (
            start_id, end_id, datetime)
        r = requests.get(url=url)
        soluzioni = r.json()['soluzioni']
        return soluzioni


class TrainInfo(ObjectType):
    orario_partenza = Field(String)
    orario_arrivo = Field(String)
    numero_treno_esteso = Field(String)


class TrainInformations(ObjectType):
    trains_info = Field(TrainInfo, train_number=String(required=True))

    def resolve_trains_info(self, info, train_number):
        print('here')
        url = 'http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/cercaNumeroTrenoTrenoAutocomplete/%s' % train_number
        r1 = requests.get(url=url)
        print(r1.text)
        start_id = r1.text[-7:]
        print(start_id)
        url = 'http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/andamentoTreno/%s/%s' % (
            start_id, train_number)
        r = requests.get(url=url)
        info_t = r.json()
        return {
            'orario_partenza': info_t.compOrarioPartenza,
            'orario_arrivo': info_t.compOrarioArrivo,
            'numero_treno_esteso': info_t.compNumeroTreno,

        }


class RootQuery(Suggestions, TrainSolutions, TrainInformations, ObjectType):
    pass


schema = Schema(query=RootQuery)
