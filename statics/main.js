window.onload = () => {
  var vue = window.vue = new Vue({
    el: '#app',
    delimiters: ['#{', '}'],
    data: {
        partenzaName: '',
        partenzaId: '',
        suggerimentiPartenza: [],
        arrivoName: '',
        arrivoId: '',
        suggerimentiArrivo: [],
        soluzioniTreni: [],
        datetime: new Date()
    },
    watch: {
        partenzaName(val){
            this.trainSuggestions(this.partenzaName, (response) => {
                this.suggerimentiPartenza = response.data['data']['suggestions']
            });
        },
        arrivoName(val){
            this.trainSuggestions(this.arrivoName, (response) => {
                this.suggerimentiArrivo = response.data['data']['suggestions']
            });
        },
    },
    methods: {
        trainSuggestions(searchString, responseHandler) {
            axios.post(
                '/graphql',
                `query{suggestions(searchString: "${searchString}"){id,name}}`,
                {
                    headers: {
                        'Content-Type': 'application/graphql'
                    }
                }
            ).then(
                (response) => {responseHandler(response)}
            ).catch((error) => {
                window.consol.log('Error: ' + error)
            });
        },
        searchTrains() {
            let datetimeString = format(this.datetime, 'yyyy-MM-ddTh:m:s');
            let partenza = this.partenzaName.substr(2);
            let arrivo = this.arrivoName.substr(2);
             axios.post(
                '/graphql',
                `query{ trains( startId:"${partenza}", endId:"${arrivo}", datetime: "${datetimeString}" ) {durata,vehicles { origine,destinazione,orarioPartenza,orarioArrivo,categoria,categoriaDescrizione,numeroTreno } } }`,
                {
                    headers: {
                        'Content-Type': 'application/graphql'
                    }
                }
            ).then(
                (response) => {
                this.soluzioniTreni = response.data['data']['trains']
            }).catch((error) => {
                window.consol.log('Error: ' + error)
            });
        }
    }
  })
}

format = function date2str(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
}